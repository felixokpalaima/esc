
<?php

//load the database configuration file
include 'dbConfig.php';
if(isset($_SESSION['user_id'] )) echo $_SESSION['user_id'];
if(!empty($_GET['status'])){
    switch($_GET['status']){
        case 'succ':
            $statusMsgClass = 'alert-success';
            $statusMsg = 'Members data has been inserted successfully.';
            break;
        case 'err':
            $statusMsgClass = 'alert-danger';
            $statusMsg = 'Some problem occurred, please try again.';
            break;
        case 'invalid_file':
            $statusMsgClass = 'alert-danger';
            $statusMsg = 'Please upload a valid CSV file.';
            break;
        default:
            $statusMsgClass = '';
            $statusMsg = '';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php
   
?>

<div class="container">
    <?php if(!empty($statusMsg)){
        echo '<div class="alert '.$statusMsgClass.'">'.$statusMsg.'</div>';
    } ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            Phone Log
            <form action="index.php" method="post">
                    select user: 
                    <input type="radio" name="privilege" value="CSO" > CSO<br>
                    <input type="radio" name="privilege" value="Data Protection Officer"> Data Protection Officer<br>
                    <input type="radio" name="privilege" value="Handler"> Handler 
                    <input type="submit" value="change" name="change">
                    </form> 

                    <?php
                    if(isset($_POST['change']))
                    { $user= $_POST['privilege'];
                    }
                    ?>
           <?php if(isset($user)  && $user=="Handler"){ ?>
            <a href="javascript:void(0);" onclick="$('#importFrm').slideToggle();">Import Phone Log</a>
           
        </div>
        <div class="panel-body">
        
            <form action="importData.php" method="post" enctype="multipart/form-data" id="importFrm">
                <input type="file" name="file" />
                <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
            </form>
            <?php } ?>
                            
            <table class="table table-bordered">
                <thead>
                    <tr>
                      <th>Number</th>
                      <th>Other ID</th>
                      <th>Direction</th>
                      <th>Llength</th>
                      <th>Duration</th>
                      <th>action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    //get records from database
                    $query = $db->query("SELECT * FROM phone_log ORDER BY id DESC");
                    if($query->num_rows > 0){ 
                        while($row = $query->fetch_assoc()){ ?>
                    <tr>
                      <td><?php echo $row['numbers']; ?></td>
                      <td><?php echo $row['other_id']; ?></td>
                      <td><?php echo $row['direction']; ?></td>
                      <td><?php echo $row['length']; ?></td>
                      <td><?php echo $row['duration'];?></td>
                      <td>
                      <?php if( isset($user) && $user==="CSO"){ echo "<a href=''>edit</a>"; }
                            else if(isset($user)  &&  $user==="Data Protection Officer"  || isset($user) && $user==="Handler")
                            { echo "<a href='index.php?id=".$row['id']."&action=delete'>delete</a> <br> <a href='edit.php?id=".$row['id']."&action=edit'>edit</a>"; }
                            else{ echo "You have only read access"; }
                      
                      
                      
                      ?></td>
                    </tr>
                    <?php } }else{ ?>
                    <tr><td colspan="5">No member(s) found.....</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<?php
  if(isset($_GET["id"]) &&  $_GET["action"]=="delete"){
      $id=$_GET["id"];
      echo $id;
    $sql = ("DELETE FROM phone_log WHERE id = ".$id);

   
if ($db->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $db->error;
}
    
    // mysqli_close($conn);
  }
?>
</body>
</html>
